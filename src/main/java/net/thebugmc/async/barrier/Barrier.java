package net.thebugmc.async.barrier;

import net.thebugmc.async.mutex.Mutex;

import java.util.concurrent.CompletableFuture;

/**
 * Barrier used to pause all threads until they all reach the same point in the code.
 *
 * <p>
 * <b>Examples</b>
 * {@snippet :
 * import net.thebugmc.async.barrier.Barrier;
 * import java.util.concurrent.atomic.AtomicInteger;
 *
 * var barrier = new Barrier(2);
 * var count = new AtomicInteger();
 *
 * var readCount = new AtomicInteger();
 *
 * var thread1 = Thread.startVirtualThread(() -> {
 *     count.getAndIncrement();
 *     barrier.await();
 * });
 * var thread2 = Thread.startVirtualThread(() -> {
 *     barrier.await();
 *     readCount.set(count.get());
 * });
 *
 * thread1.join();
 * thread2.join();
 *
 * assertEquals(1, readCount.get());
 *}
 */
public class Barrier {
    private final int threadCount;

    /**
     * Create a new barrier expecting any {@link #threadCount} threads to call {@link #await()}
     * every time you want those threads to be synchronized.
     *
     * @param threadCount The number of threads that should call {@link #await()}.
     */
    public Barrier(int threadCount) {
        this.threadCount = threadCount;
    }

    private CompletableFuture<?> currentFuture = new CompletableFuture<>();
    private final Mutex<Integer> currentCount = Mutex.of(0);

    /**
     * Get the number of threads that should call {@link #await()} every time.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.barrier.Barrier;
     *
     * var barrier = new Barrier(2);
     * assertEquals(2, barrier.threadCount());
     *}
     *
     * @return The number of threads that should call {@link #await()}.
     */
    public int threadCount() {
        return threadCount;
    }

    /**
     * Stand at this barrier call waiting for all other threads to reach the same point in the code.
     *
     * <p>
     * See {@link Barrier} for examples.
     */
    public void await() {
        try (var count = currentCount.access()) {
            if (count.set(count.get() + 1) == threadCount - 1) {
                var future = currentFuture;
                currentFuture = new CompletableFuture<>();
                future.complete(null);
                return;
            }
        }

        currentFuture.join();
    }
}
