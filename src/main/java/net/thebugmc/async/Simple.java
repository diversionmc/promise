package net.thebugmc.async;

import java.util.function.Function;

/**
 * Ensure that {@link Promise#map(Function)} will try to pass the result through.
 *
 * <p>
 * The implementors (lambdas) shouldn't run any heavy operations that would rather be run in another
 * thread, and definitely should not perform any blocking operations.
 */
@FunctionalInterface
public interface Simple<T, R> extends Function<T, R> {
}
