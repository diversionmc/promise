package net.thebugmc.async.schedule;

import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.lang.Math.max;
import static java.lang.System.nanoTime;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static net.thebugmc.async.schedule.VirtualThreadScheduler.SleepResult.*;

/**
 * A scheduler that performs action in its own sustained threads.
 */
public final class VirtualThreadScheduler implements Scheduler, AutoCloseable {
    //
    // Scheduler instance
    //

    /**
     * Instance of this thread pool scheduler.
     *
     * <p>
     * In the future, this will be a {@code ScopedValue} that will pull down the scheduler from the
     * parent that creates an {@link #instance()} of {@code VirtualThreadScheduler}.
     */
    private static final ThreadLocal<Optional<VirtualThreadScheduler>> CONTEXT
        = ThreadLocal.withInitial(Optional::empty);

    /**
     * Get or create a new instance of {@link VirtualThreadScheduler} for the current thread.
     *
     * <p>
     * Tasks scheduled with {@link VirtualThreadScheduler} already have an instance from the parent,
     * so they will get that. In many other places, this will return a newly created instance.
     *
     * @return True if current thread is a virtual thread.
     */
    public static VirtualThreadScheduler instance() {
        return instance(VirtualThreadScheduler::new);
    }

    /**
     * Get or grab supplied {@link VirtualThreadScheduler} for the current thread.
     *
     * <p>
     * Tasks scheduled with {@link VirtualThreadScheduler} already have an instance from the parent,
     * so they will get that. In many other places, this will return a newly created instance.
     *
     * @return True if current thread is a virtual thread.
     */
    public static VirtualThreadScheduler instance(Supplier<VirtualThreadScheduler> defaultInstance) {
        // this code works only because it's a thread-local, so there cannot be any races
        var current = CONTEXT.get();
        if (current.isEmpty())
            CONTEXT.set(current = Optional.of(defaultInstance.get()));
        return current.get();
    }

    private VirtualThreadScheduler() {
    }

    //
    // Synchronization
    //

    /**
     * Predefined amount of time that {@link #close()} will wait for all tasks to finish.
     */
    public static final long AWAIT_TIMEOUT_MILLIS = 5000;

    private final CompletableFuture<?> shutdownFuture = new CompletableFuture<>();

    /**
     * Stops all tasks in this {@link VirtualThreadScheduler} and waits for them to finish.
     */
    public void close() {
        CONTEXT.set(Optional.empty());

        // notify users that they should stop
        shutdownFuture.complete(null);

        // try to shutdown
        executorService.shutdown();

        // have only one await
        var awaitDone = false;
        try {
            awaitDone = executorService.awaitTermination(AWAIT_TIMEOUT_MILLIS, MILLISECONDS);
        } catch (InterruptedException ignored) {
        }

        // send an interrupt (or similar) if not done, and then just proceed as if tasks shut down
        if (!awaitDone) executorService.shutdownNow();
    }

    /**
     * Result of a {@link #sleep(long, TimeUnit)} call.
     */
    public sealed interface SleepResult permits SleepComplete, Interrupted, ShuttingDown {
        /**
         * Successful sleep. The entire sleep period has been completed.
         */
        record SleepComplete() implements SleepResult {
        }

        /**
         * Sleep interrupted. Sleep period may have not been completed fully.
         *
         * @param interrupted {@link VirtualThreadScheduler} that was interrupted.
         *                    This will be the scheduler that will be used i
         *                    {@link #sleep(long, TimeUnit)} when you do {@link #goBackToSleep()}.
         * @param remaining   Time that was left to wait for.
         * @param unit        Unit of time that was left.
         */
        record Interrupted(
            VirtualThreadScheduler interrupted,
            long remaining,
            TimeUnit unit
        ) implements SleepResult {
            /**
             * Try to complete the interrupted sleep.
             *
             * @return {@link SleepResult}
             */
            public SleepResult goBackToSleep() {
                return interrupted.sleep(remaining, unit);
            }
        }

        /**
         * Sleep cannot be completed because the {@link VirtualThreadScheduler} has requested all
         * tasks to finish swiftly.
         */
        record ShuttingDown() implements SleepResult {
        }
    }

    /**
     * Try sleeping for the specified amount of time.
     *
     * @param timeout Time to sleep for.
     * @param unit    Sleeping time unit.
     * @return {@link SleepResult}
     */
    public SleepResult sleep(long timeout, TimeUnit unit) {
        var start = nanoTime();
        try {
            shutdownFuture.get(timeout, unit);
            return new ShuttingDown();
        } catch (InterruptedException ignored) {
            return new Interrupted(
                this,
                NANOSECONDS.convert(max(nanoTime() - start, 0), unit),
                unit
            );
        } catch (TimeoutException ignored) {
            return new SleepComplete();
        } catch (ExecutionException e) {
            throw new AssertionError("Unreachable exception", e);
        }
    }

    //
    // Execution
    //

    private final ExecutorService executorService = Executors.newVirtualThreadPerTaskExecutor();

    private static final ThreadLocal<Optional<VirtualThreadScheduler>> OWNER
        = ThreadLocal.withInitial(Optional::empty);

    /**
     * Get the {@link VirtualThreadScheduler} that the {@link Thread#currentThread() current thread}
     * was created with.
     *
     * @return Some if current thread is a thread made by some instance of a
     * {@link VirtualThreadScheduler}, none otherwise.
     */
    public static Optional<VirtualThreadScheduler> owner() {
        return OWNER.get();
    }

    public Future<?> schedule(Runnable task) {
        return executorService.submit(() -> {
            OWNER.set(Optional.of(this));
            CONTEXT.set(Optional.of(this));
            task.run();
        });
    }

    public Future<?> scheduleDelay(long delay, TimeUnit unit) {
        return schedule(() -> {
            var ignored = sleep(delay, unit);
            // interrupted or shutdown sleep should just finish earlier
        });
    }

    public Future<?> scheduleRepeating(
        long period,
        TimeUnit unit,
        Consumer<LoopState> task
    ) {
        return schedule(() -> {
            var iter = new AtomicInteger();
            var loop = new LoopState(iter);
            while (true) {
                long start = nanoTime();

                try {
                    task.accept(loop);
                } catch (Throwable t) {
                    loop.stop(); // mark as stopped for non-pure function leaky bois
                    //noinspection ProhibitedExceptionThrown
                    throw t;
                }

                if (loop.stopped()) break;

                long time = NANOSECONDS.convert(period, unit);
                long delta = nanoTime() - start;
                if (delta < time)
                    if (sleep(time - delta, NANOSECONDS) instanceof ShuttingDown) {
                        loop.stop();
                        break;
                    }
                // any other sleep result should be ignored and go to the next iteration

                iter.getAndIncrement();
            }
        });
    }

    //
    // TryParent
    //

    /**
     * A scheduler that tries to pull a parent {@link VirtualThreadScheduler}.
     */
    public static final class TryParent implements Scheduler {
        public static final TryParent INSTANCE = new TryParent();

        public static TryParent instance() {
            return INSTANCE;
        }

        /**
         * Exception that occurs when using this scheduler outside a VirtualThreadScheduler task.
         */
        public static final class ParentVirtualThreadSchedulerNotFoundException extends RuntimeException {
            public ParentVirtualThreadSchedulerNotFoundException() {
                super(
                    "Expected for this to be called in a VirtualThreadScheduler containing "
                    + "thread. If you are trying to enter the Promise-land, try making a "
                    + "VirtualThreadScheduler by calling `instance()`, but make sure to close (or "
                    + "auto-close) it whenever you are ready for a shutdown."
                );
            }
        }

        /**
         * Get the {@link VirtualThreadScheduler} that this {@link TryParent} tries to pull from.
         */
        public Optional<VirtualThreadScheduler> context() {
            return CONTEXT.get();
        }

        public VirtualThreadScheduler get() {
            return context()
                .orElseThrow(ParentVirtualThreadSchedulerNotFoundException::new);
        }

        public Future<?> schedule(Runnable task) {
            return get().schedule(task);
        }

        public Future<?> scheduleDelay(long delay, TimeUnit unit) {
            return get().scheduleDelay(delay, unit);
        }

        public Future<?> scheduleRepeating(
            long period,
            TimeUnit unit,
            Consumer<LoopState> task
        ) {
            return get().scheduleRepeating(period, unit, task);
        }
    }
}
