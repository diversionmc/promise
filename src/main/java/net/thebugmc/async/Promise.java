package net.thebugmc.async;

import net.thebugmc.async.schedule.LoopState;
import net.thebugmc.async.schedule.Scheduler;
import net.thebugmc.async.schedule.VirtualThreadScheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.Map.entry;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * <p>
 * Promise is an async result supplier. The type of result is either defined or {@code ?} (always
 * {@code null}).
 *
 * <p>
 * This API is inspired by Java's {@link Optional Optional API} and {@link Stream Stream API} and
 * ECMAScript's <i>Promises API</i>. Most of the features are made to be on parity with Optionals
 * and Streams (and the method names are similar), and the main idea is same as in ECMAScript's
 * version (but the method names are different).
 *
 * <p>
 * Promises return the result some time later after its creation. A currently evaluating promise is
 * {@link #pending() pending}, and after it is done it is {@link #settled() settled}.
 *
 * <p>
 * Promises can be chained.
 * The awaited promise is called a <i>parent</i>, and following promises use the result from the
 * parent. A promise that has no parent is <i>independent</i>. Chaining does not necessarily mean
 * order of operations (although it is often the case), but rather that making a step is allowed
 * only after the parent promise is settled.
 *
 * <p>
 * There are 5 types of methods promises supply:
 * <ul>
 *     <li>Start methods - generate new independent promises.</li>
 *     <li>Modify methods - make promises that use the result from the parent.</li>
 *     <li>Then (new start) methods - generate new start promises with parents.</li>
 *     <li>Flat modify methods - generate new promises in a function and use their result.</li>
 *     <li>Flat then/start methods - generate new start promises in a function.</li>
 * </ul>
 * Methods also divide into <i>passthrough</i> and <i>new result</i>. Passthrough methods are
 * guaranteed to return the result of parent chain.
 *
 * <p>
 * The start method types are as follows:
 * <ul>
 *     <li>Passthrough (when chained):<ul>
 *         <li>{@link #run(Runnable) run} - Starts a new promise with no result.</li>
 *         <li>{@link #after(Promise[]) after} - Awaits for supplied promises group.</li>
 *         <li>{@link #repeat(long, TimeUnit, Consumer) repeat} - Creates a repeating schedule
 *         promise.</li>
 *     </ul></li>
 *     <li>New result:<ul>
 *         <li>{@link #of(Object) of} - Makes a settled promise with a result.</li>
 *         <li>{@link #reference() reference} (no {@code thenReference}) - Creates a promise that
 *         you may settle from anywhere else.</li>
 *         <li>{@link #get(Supplier) get} - Starts a new promise with a result supplied.</li>
 *         <li>{@link #batch(Promise[]) batch} - Awaits for supplied promises group results of a
 *         common type.</li>
 *     </ul></li>
 * </ul>
 * Promise groups are evaluated in parallel.
 *
 * <p>
 * The modify method types are as follows:
 * <ul>
 *     <li>New result:<ul>
 *         <li>{@link #map(Function) map} - Gives a new result promise using parent result.</li>
 *     </ul></li>
 *     <li>Passthrough:<ul>
 *         <li>{@link #peek(Consumer) peek} - Performs an action using the parent result and passes
 *         that result over.</li>
 *     </ul></li>
 * </ul>
 *
 * @param <R> Resulting object type, or ? if promise supplies no result.
 */
public final class Promise<R> implements Future<R> {
    public static final int MINECRAFT_TICKS_TO_MILLIS = 50;

    //
    // Future
    //

    private static <T> T getUninterruptably(Future<T> future) throws ExecutionException {
        var interrupted = false;
        try {
            while (true) try {
                if (future instanceof Promise<?> promise)
                    promise.awaited.set(true);
                return future.get();
            } catch (InterruptedException e) {
                interrupted = true;
            }
        } finally {
            if (interrupted)
                Thread.currentThread().interrupt();
        }
    }

    private static <T> T getUninterruptably(
        Future<T> future,
        long timeout, TimeUnit unit
    ) throws ExecutionException, TimeoutException {
        var interrupted = false;
        try {
            while (true) try {
                if (future instanceof Promise<?> promise)
                    promise.awaited.set(true);
                return future.get(timeout, unit);
            } catch (InterruptedException e) {
                interrupted = true;
            }
        } finally {
            if (interrupted)
                Thread.currentThread().interrupt();
        }
    }

    private final CompletableFuture<R> future;
    private final AtomicBoolean awaited = new AtomicBoolean();

    public boolean cancel(boolean mayInterruptIfRunning) {
        return future.cancel(mayInterruptIfRunning);
    }

    public boolean isCancelled() {
        return future.isCancelled();
    }

    public boolean isDone() {
        return future.isDone();
    }

    public R get() throws PromiseException {
        try {
            return getUninterruptably(future);
        } catch (ExecutionException e) {
            throw new PromiseException(e.getCause());
        }
    }

    public R get(
        long timeout, TimeUnit unit
    ) throws PromiseException, TimeoutException {
        try {
            return getUninterruptably(future, timeout, unit);
        } catch (ExecutionException e) {
            throw new PromiseException(e.getCause());
        }
    }

    //
    // Task and waiting
    //

    @SuppressWarnings("OverlyBroadCatchBlock")
    private <T> Promise(
        Scheduler scheduler,
        Optional<Future<T>> parent,
        Function<T, R> task
    ) {
        future = new CompletableFuture<>();

        var vts = VirtualThreadScheduler.TryParent.INSTANCE;

        // shortcut to preserve settled status
        if (parent.isPresent() && parent.get().isDone())
            try {
                T parentValue = getUninterruptably(parent.get());

                if (task instanceof Simple) {
                    future.complete(task.apply(parentValue));
                    return;
                }

                vts.schedule(() -> {
                    try {
                        getUninterruptably(
                            scheduler.schedule(() -> future.complete(task.apply(parentValue)))
                        );
                    } catch (Throwable e) {
                        future.completeExceptionally(e);
                    }
                });
                return;
            } catch (Throwable e) {
                future.completeExceptionally(e);
                return;
            }

        vts.schedule(() -> {
            try {
                T parentValue = null;
                if (parent.isPresent())
                    parentValue = getUninterruptably(parent.get());

                T finalParentValue = parentValue;
                getUninterruptably(
                    scheduler.schedule(() -> future.complete(task.apply(finalParentValue)))
                );
            } catch (Throwable e) {
                future.completeExceptionally(e);
            }
        });

        // print errors if nobody listening
        vts.schedule(() -> {
            try {
                getUninterruptably(future);
            } catch (ExecutionException e) {
                vts.context().get().sleep(1, TimeUnit.SECONDS);
                if (!awaited.get()) e.printStackTrace();
            }
        });
    }

    private Promise(Consumer<Consumer<R>> complete) {
        future = new CompletableFuture<>();
        complete.accept(result -> {
            if (!future.complete(result))
                throw new PromiseException(new IllegalStateException(
                    "Attempt to resolve a reference promise more than once"
                ));
        });
    }

    private Promise(R result) {
        future = completedFuture(result);
    }

    private Promise(CompletableFuture<R> future) {
        this.future = future;
    }

    /**
     * Await for promise to finish by blocking this thread.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     *
     * var ref = Promise.<String>reference();
     * var promise = ref.getKey();
     * var setter = ref.getValue();
     *
     * var thread1 = Thread.startVirtualThread(() -> setter.accept("hello"));
     * var thread2 = Thread.startVirtualThread(() -> {
     *     var result = promise.await();
     *     assertEquals("hello", result);
     * });
     *}
     *
     * @return Promise result.
     * @throws PromiseException When an underlying execution exception occurs.
     */
    public R await() throws PromiseException {
        return get();
    }

    /**
     * Try to get the result if this promise has settled.
     *
     * <p>
     * This may return an {@link Optional#empty() empty} value if the result is {@code null} or the
     * underlying task has failed. Use {@link #settled()} to check if the task has actually
     * completed.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     *
     * var ref = Promise.<String>reference();
     * var promise = ref.getKey();
     * var setter = ref.getValue();
     *
     * assertTrue(promise.result().isEmpty());
     * setter.accept("hello");
     * assertEquals("hello", promise.result().get());
     *}
     *
     * @return Optional of the result of the promise.
     */
    public Optional<R> result() {
        // pending check for passthrough promises that have not completed yet but set result already
        return pending()
            ? empty()
            : ofNullable(resultNow());
    }

    /**
     * Check if promise is not done evaluating the result yet.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     *
     * var ref = Promise.<String>reference();
     * var promise = ref.getKey();
     * assertTrue(promise.pending());
     *}
     *
     * @return Pending state.
     */
    public boolean pending() {
        return !future.isDone();
    }

    /**
     * Check if promise is no longer {@link #pending() pending}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     *
     * var promise = Promise.of("hello");
     * assertTrue(promise.settled());
     *}
     *
     * @return Settled state.
     */
    public boolean settled() {
        return !pending();
    }

    //
    // Start
    //

    // <?>

    // run()

    /**
     * Start a new promise without results.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.concurrent.atomic.AtomicReference;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var data = new AtomicReference<String>();
     *     var promise = Promise.run(() -> data.set("hello"));
     *     promise.await();
     *     assertEquals("hello", data.get());
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public static Promise<?> run(Runnable handle) {
        return run(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Start a new promise without results. This is a scheduler version of {@link #run(Runnable)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     */
    public static Promise<?> run(
        Scheduler scheduler,
        Runnable handle
    ) {
        return new Promise<>(
            scheduler,
            empty(),
            from -> {
                handle.run();
                return from;
            }
        );
    }

    // after()

    /**
     * Start a new promise waiting for a promise group to finish.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import net.thebugmc.async.barrier.Barrier;
     * import java.util.ArrayList;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var list = new ArrayList<>();
     *     var barrier = new Barrier(2);
     *     var after = Promise.after(
     *         Promise.run(() -> {
     *             list.add("hello");
     *             barrier.await();
     *         }),
     *         Promise.run(() -> {
     *             barrier.await();
     *             list.add("world");
     *         })
     *     );
     *     after.await();
     *     assertEquals(List.of("hello", "world"), list);
     * }
     *}
     *
     * @param promises Promises to wait for.
     */
    public static Promise<?> after(Promise<?>... promises) {
        return after(stream(promises));
    }

    /**
     * Start a new promise waiting for a promise group to finish. This is a collection version of
     * {@link #after(Promise[])}.
     *
     * @param promises Promises to wait for.
     */
    public static Promise<?> after(Collection<Promise<?>> promises) {
        return after(promises.stream());
    }

    /**
     * Start a new promise waiting for a promise group to finish. This is a stream version of
     * {@link #after(Promise[])}.
     *
     * @param promises Promises to wait for.
     */
    public static Promise<?> after(Stream<Promise<?>> promises) {
        //noinspection ResultOfMethodCallIgnored
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            empty(),
            from -> {
                promises
                    .map(Promise::await) // ensure await() happens inside the asyncAction block
                    .toList();           // by calling toList and hence terminating the stream
                return from;
            }
        );
    }

    // repeat()

    /**
     * Start a new repeating schedule promise. This is a Minecraft tick version of
     * {@link #repeat(long, TimeUnit, Consumer)}.
     *
     * @param ticksPeriod Period between iterations, in Minecraft ticks (1/20 second).
     * @param handle      Action to perform.
     */
    public static Promise<?> repeat(
        long ticksPeriod,
        Consumer<LoopState> handle
    ) {
        return repeat(ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS, handle);
    }

    /**
     * Start a new repeating schedule promise.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.concurrent.atomic.AtomicInteger;
     * import static java.util.concurrent.TimeUnit.*;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var data = new AtomicInteger();
     *     var start = System.currentTimeMillis();
     *     var promise = Promise.repeat(
     *         1, MILLISECONDS,
     *         i -> {
     *             if (!(i.iteration() < 2)) { // stop at iteration #2 (0-1, 2 iters)
     *                 i.stop();
     *                 return;
     *             }
     *             data.incrementAndGet();
     *         }
     *     );
     *     promise.await();
     *     var end = System.currentTimeMillis();
     *     assertEquals(2, data.get());
     *     assertTrue(end - start >= 2);
     * }
     *}
     *
     * @param period Period between iterations.
     * @param unit   Period unit.
     * @param handle Action to perform.
     */
    public static Promise<?> repeat(
        long period, TimeUnit unit,
        Consumer<LoopState> handle
    ) {
        return repeat(VirtualThreadScheduler.TryParent.INSTANCE, period, unit, handle);
    }

    /**
     * Start a new repeating schedule promise. This is a Minecraft tick scheduler version of
     * {@link #repeat(long, TimeUnit, Consumer)}.
     *
     * @param scheduler   Thread supplier to run on.
     * @param ticksPeriod Period between iterations, in Minecraft ticks (1/20 second).
     * @param handle      Action to perform.
     */
    public static Promise<?> repeat(
        Scheduler scheduler,
        long ticksPeriod,
        Consumer<LoopState> handle
    ) {
        return repeat(scheduler, ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS, handle);
    }

    /**
     * Start a new repeating schedule promise. This is a scheduler version of
     * {@link #repeat(long, TimeUnit, Consumer)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param period    Period between iterations.
     * @param unit      Period unit.
     * @param handle    Action to perform.
     */
    public static Promise<?> repeat(
        Scheduler scheduler,
        long period, TimeUnit unit,
        Consumer<LoopState> handle
    ) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            empty(),
            from -> {
                try {
                    scheduler
                        .scheduleRepeating(period, unit, handle)
                        .get();
                    // wait for scheduleRepeating to finish for current promise to count as
                    // completed
                } catch (InterruptedException ignored) {
                    // fine to ignore since repeat gives no result
                } catch (ExecutionException e) {
                    throw new PromiseException(e.getCause());
                }
                return from;
            }
        );
    }

    // delay()

    /**
     * Promise that awaits for a certain amount of time. This is a Minecraft tick version of
     * {@link #delay(long, TimeUnit)}.
     *
     * @param ticksPeriod Period to await for, in Minecraft ticks (1/20 second).
     */
    public static Promise<?> delay(long ticksPeriod) {
        return delay(ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS);
    }

    /**
     * Promise that awaits for a certain amount of time.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import static java.util.concurrent.TimeUnit.*;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var start = System.currentTimeMillis();
     *     var promise = Promise.delay(3, MILLISECONDS);
     *     promise.await();
     *     var end = System.currentTimeMillis();
     *     assertTrue(end - start >= 3);
     * }
     *}
     *
     * @param period Period to await for.
     * @param unit   Period unit.
     */
    public static Promise<?> delay(long period, TimeUnit unit) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            empty(),
            from -> {
                try {
                    VirtualThreadScheduler
                        .TryParent
                        .INSTANCE
                        .scheduleDelay(period, unit)
                        .get();
                    return from;
                } catch (InterruptedException e) {
                    throw new PromiseException(e);
                } catch (ExecutionException e) {
                    throw new PromiseException(e.getCause());
                }
            }
        );
    }

    // <N>

    // of()

    /**
     * Skip starting anything and just get a {@link #settled settled} promise of given value.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import java.util.Optional;
     *
     * var promise = Promise.of("hello");
     * assertEquals(Optional.of("hello"), promise.result());
     *}
     *
     * @param value Value to put inside.
     * @param <N>   New result type.
     */
    public static <N> Promise<N> of(N value) {
        return new Promise<>(value);
    }

    // from()

    /**
     * Convert a {@link Future} into a {@link Promise}.
     *
     * @param future Future to convert.
     * @return Promise wrapping the future.
     */
    public static <R> Promise<R> from(Future<R> future) {
        if (future instanceof Promise<R> promise) return promise;
        if (future instanceof CompletableFuture<R> fut) return new Promise<>(fut);
        return Promise.get(() -> {
            try {
                return getUninterruptably(future);
            } catch (ExecutionException e) {
                throw new PromiseException(e);
            }
        });
    }

    // get()

    /**
     * Start a new promise giving a result.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     assertEquals("hello", promise.await());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public static <N> Promise<N> get(Supplier<N> handle) {
        return get(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Start a new promise giving a result. This is a scheduler version of {@link #get(Supplier)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     * @param <N>       New result type.
     */
    public static <N> Promise<N> get(Scheduler scheduler, Supplier<N> handle) {
        return new Promise<>(
            scheduler,
            empty(),
            from -> handle.get()
        );
    }

    // reference()

    /**
     * Create a promise that will be resolved by any thread that calls the setter.
     *
     * <p>
     * See {@link #await()} for an example.
     *
     * @param <N> The reference type.
     * @return A pair of a promise and a function that will settle that promise.
     */
    public static <N> Entry<Promise<N>, Consumer<N>> reference() {
        var atomic = new AtomicReference<Consumer<N>>();
        var promise = new Promise<>(atomic::set);
        return entry(promise, atomic.get());
    }

    // batch()

    /**
     * Start a promise waiting for results from a promise group of a common type.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.batch(
     *         Promise.get(() -> "hello"),
     *         Promise.get(() -> "world")
     *     );
     *     assertEquals(List.of("hello", "world"), promise.await().toList());
     * }
     *}
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    @SafeVarargs
    public static <N> Promise<Stream<N>> batch(Promise<N>... promises) {
        return batch(stream(promises));
    }

    /**
     * Start a promise waiting for results from a promise group of a common type. This is a
     * collection version of {@link #batch(Promise[])}.
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    public static <N> Promise<Stream<N>> batch(Collection<Promise<N>> promises) {
        return batch(promises.stream());
    }

    /**
     * Start a promise waiting for results from a promise group of a common type. This is a
     * stream version of {@link #batch(Promise[])}.
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    public static <N> Promise<Stream<N>> batch(Stream<Promise<N>> promises) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            empty(),
            from -> promises
                .map(Promise::await) // ensure await() happens inside the asyncAction block
                .toList().stream() // by calling toList and hence terminating the stream
        );
    }

    //
    // Modify result
    //

    // <N>

    // map()

    /**
     * Transform a value.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.map(s -> s + " world");
     *     assertEquals("hello world", promise2.await());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public <N> Promise<N> map(Function<R, N> handle) {
        return map(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Transform a value. This is a scheduler version of {@link #map(Function)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     * @param <N>       New result type.
     */
    public <N> Promise<N> map(Scheduler scheduler, Function<R, N> handle) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            handle
        );
    }

    // handle()

    /**
     * Make a promise that handles an exception thrown by this promise.
     *
     * @param handle Action to perform.
     * @return The new promise.
     */
    public <N> Promise<N> handle(Function<R, N> map, Function<Throwable, N> handle) {
        return get(() -> {
            try {
                return map.apply(await());
            } catch (PromiseException e) {
                return handle.apply(e.getCause());
            }
        });
    }

    /**
     * Make a promise that handles an exception thrown by this promise.
     *
     * @param errorClass Exception class to handle.
     * @param handle     Action to perform.
     * @return The new promise.
     */
    public <N, E extends Throwable> Promise<N> handle(
        Function<R, N> map,
        Class<E> errorClass,
        Function<E, N> handle
    ) {
        return get(() -> {
            try {
                return map.apply(await());
            } catch (PromiseException e) {
                var cause = e.getCause();
                if (errorClass.isInstance(cause))
                    //noinspection unchecked
                    return handle.apply((E) cause);
                throw e;
            }
        });
    }

    // <R>

    // peek()

    /**
     * Perform an action using result of this promise and pass the result over.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.concurrent.atomic.AtomicReference;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var str = new AtomicReference<>();
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.peek(str::set);
     *     promise2.await();
     *     assertEquals("hello", str.get());
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public Promise<R> peek(Consumer<R> handle) {
        return peek(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Perform an action using result of this promise and pass the result over. This is a scheduler
     * version of {@link #peek(Consumer)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     */
    public Promise<R> peek(Scheduler scheduler, Consumer<R> handle) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            from -> {
                handle.accept(from);
                return from;
            }
        );
    }

    //
    // Discard result
    //

    // <R>

    // thenRun()

    /**
     * Start a new promise without results after this promise is {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.ArrayList;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var list = new ArrayList<>();
     *     var promise = Promise.get(() -> list.add("hello"));
     *     var promise2 = promise.thenRun(() -> list.add("world"));
     *     promise2.await();
     *     assertEquals(List.of("hello", "world"), list);
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public Promise<R> thenRun(Runnable handle) {
        return thenRun(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Start a new promise without results after this promise is {@link #settled() settled}. This is
     * a scheduler version of {@link #thenRun(Runnable)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     */
    public Promise<R> thenRun(Scheduler scheduler, Runnable handle) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            from -> {
                handle.run();
                return from;
            }
        );
    }

    // thenAfter()

    /**
     * Start a new promise waiting for a promise group to finish after this promise is
     * {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.thenAfter(Promise.get(() -> "world"));
     *     assertEquals("hello", promise2.await());
     * }
     *}
     *
     * @param promises Promises to wait for.
     */
    public Promise<R> thenAfter(Promise<?>... promises) {
        return thenAfter(stream(promises));
    }

    /**
     * Start a new promise waiting for a promise group to finish after this promise is
     * {@link #settled() settled}. This is a collection version of {@link #thenAfter(Promise[])}.
     *
     * @param promises Promises to wait for.
     */
    public Promise<R> thenAfter(Collection<Promise<?>> promises) {
        return thenAfter(promises.stream());
    }

    /**
     * Start a new promise waiting for a promise group to finish after this promise is
     * {@link #settled() settled}. This is a stream version of {@link #thenAfter(Promise[])}.
     *
     * @param promises Promises to wait for.
     */
    public Promise<R> thenAfter(Stream<Promise<?>> promises) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> {
                //noinspection ResultOfMethodCallIgnored
                promises
                    .map(Promise::await) // ensure await() happens inside the asyncAction block
                    .toList();           // by calling toList and hence terminating the stream
                return from;
            }
        );
    }

    // thenRepeat()

    /**
     * Start a new repeating schedule promise after this promise is {@link #settled() settled}. This
     * is a Minecraft tick version of {@link #thenRepeat(long, TimeUnit, Consumer)}.
     *
     * @param ticksPeriod Period between iterations, in Minecraft ticks (1/20 second).
     * @param handle      Action to perform.
     */
    public Promise<R> thenRepeat(long ticksPeriod, Consumer<LoopState> handle) {
        return thenRepeat(ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS, handle);
    }

    /**
     * Start a new repeating schedule promise after this promise is {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import static java.util.concurrent.TimeUnit.*;
     * import java.util.concurrent.atomic.AtomicInteger;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var start = System.currentTimeMillis();
     *     var iters = new AtomicInteger();
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.thenRepeat(1, MILLISECONDS, i -> {
     *         if (!(i.iteration() < 2)) { // stop at iteration #2 (0-1, 2 iters)
     *             i.stop();
     *             return;
     *         }
     *         iters.incrementAndGet();
     *     });
     *     assertEquals("hello", promise2.await());
     *     assertEquals(2, iters.get());
     *     var end = System.currentTimeMillis();
     *     assertTrue(end - start >= 2);
     * }
     *}
     *
     * @param period Period between iterations.
     * @param unit   Period unit.
     * @param handle Action to perform.
     */
    public Promise<R> thenRepeat(long period, TimeUnit unit, Consumer<LoopState> handle) {
        return thenRepeat(VirtualThreadScheduler.TryParent.INSTANCE, period, unit, handle);
    }

    /**
     * Start a new repeating schedule promise after this promise is {@link #settled() settled}. This
     * is a scheduler version of {@link #thenRepeat(long, Consumer)}.
     *
     * @param scheduler   Thread supplier to run on.
     * @param ticksPeriod Period between iterations, in Minecraft ticks (1/20 second).
     * @param handle      Action to perform.
     */
    public Promise<R> thenRepeat(
        Scheduler scheduler,
        long ticksPeriod,
        Consumer<LoopState> handle
    ) {
        return thenRepeat(scheduler, ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS, handle);
    }

    /**
     * Start a new repeating schedule promise after this promise is {@link #settled() settled}. This
     * is a scheduler version of {@link #thenRepeat(long, TimeUnit, Consumer)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param period    Period between iterations.
     * @param unit      Period unit.
     * @param handle    Action to perform.
     */
    public Promise<R> thenRepeat(
        Scheduler scheduler,
        long period,
        TimeUnit unit,
        Consumer<LoopState> handle
    ) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            from -> {
                try {
                    scheduler
                        .scheduleRepeating(period, unit, handle)
                        .get();
                    // and wait for scheduleRepeating to finish for current submit to count as
                    // completed
                } catch (InterruptedException ignored) {
                    // fine to ignore since repeat gives no result
                } catch (ExecutionException e) {
                    throw new PromiseException(e.getCause());
                }
                return from;
            }
        );
    }

    // thenDelay()

    /**
     * Promise that awaits for a certain amount of time after this promise is
     * {@link #settled() settled}. This is a Minecraft tick version of
     * {@link #thenDelay(long, TimeUnit)}.
     *
     * @param ticksPeriod Period to await for, in Minecraft ticks (1/20 second).
     */
    public Promise<R> thenDelay(long ticksPeriod) {
        return thenDelay(ticksPeriod * MINECRAFT_TICKS_TO_MILLIS, MILLISECONDS);
    }

    /**
     * Promise that awaits for a certain amount of time after this promise is
     * {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import static java.util.concurrent.TimeUnit.*;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var start = System.currentTimeMillis();
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.thenDelay(3, MILLISECONDS);
     *     assertEquals("hello", promise2.await());
     *     var end = System.currentTimeMillis();
     *     assertTrue(end - start >= 3);
     * }
     *}
     *
     * @param period Period to await for.
     * @param unit   Period unit.
     */
    public Promise<R> thenDelay(long period, TimeUnit unit) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> {
                try {
                    VirtualThreadScheduler
                        .TryParent
                        .INSTANCE
                        .scheduleDelay(period, unit)
                        .get();
                } catch (InterruptedException ignored) {
                } catch (ExecutionException e) {
                    throw new PromiseException(e.getCause());
                }
                return from;
            }
        );
    }

    // <N>

    // thenOf()

    /**
     * Start a new promise giving a result after this promise is {@link #settled() settled}.
     *
     * <p>
     * Note that an {@link #await()} or similar will still be needed, as {@code this} promise will
     * still need to {@link #settled() settle} before {@code thenOf} <i>can</i> kick in.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.of("hello");
     *     var promise2 = promise.thenOf("world");
     *     assertEquals("world", promise2.await());
     * }
     *}
     *
     * @param value Value to put inside.
     * @param <N>   New result type.
     */
    public <N> Promise<N> thenOf(N value) {
        return thenOf(VirtualThreadScheduler.TryParent.INSTANCE, value);
    }

    /**
     * Start a new promise giving a result after this promise is {@link #settled() settled}. This is
     * a scheduler version of {@link #thenOf(Object)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param value     Value to put inside.
     * @param <N>       New result type.
     */
    public <N> Promise<N> thenOf(Scheduler scheduler, N value) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            from -> value
        );
    }

    // thenGet()

    /**
     * Start a new promise giving a result after this promise is {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.thenGet(() -> "world");
     *     assertEquals("world", promise2.await());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public <N> Promise<N> thenGet(Supplier<N> handle) {
        return thenGet(VirtualThreadScheduler.TryParent.INSTANCE, handle);
    }

    /**
     * Start a new promise giving a result after this promise is {@link #settled() settled}. This is
     * a scheduler version of {@link #thenGet(Supplier)}.
     *
     * @param scheduler Thread supplier to run on.
     * @param handle    Action to perform.
     * @param <N>       New result type.
     */
    public <N> Promise<N> thenGet(Scheduler scheduler, Supplier<N> handle) {
        return new Promise<>(
            scheduler,
            Optional.of(this),
            from -> handle.get()
        );
    }

    // thenBatch()

    /**
     * Start a promise waiting for results from a promise group of a common type after this promise
     * is {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.thenBatch(
     *         Promise.get(() -> "hello"),
     *         Promise.get(() -> "world")
     *     );
     *     assertEquals(List.of("hello", "world"), promise2.await().toList());
     * }
     *}
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    @SafeVarargs // SafeVarargs requires method to be final, even though the class itself is final
    public final <N> Promise<Stream<N>> thenBatch(Promise<N>... promises) {
        return thenBatch(stream(promises));
    }

    /**
     * Start a promise waiting for results from a promise group of a common type after this promise
     * is {@link #settled() settled}. This is a collection version of {@link #thenBatch(Promise[])}.
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    public <N> Promise<Stream<N>> thenBatch(Collection<Promise<N>> promises) {
        return thenBatch(promises.stream());
    }

    /**
     * Start a promise waiting for results from a promise group of a common type after this promise
     * is {@link #settled() settled}. This is a stream version of {@link #thenBatch(Promise[])}.
     *
     * @param promises Promises to wait for.
     * @param <N>      New result type.
     */
    public <N> Promise<Stream<N>> thenBatch(Stream<Promise<N>> promises) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> promises
                .map(Promise::await) // ensure await() happens inside the asyncAction block
                .toList().stream()   // by calling toList and hence terminating the stream
        );
    }

    //
    // Flat modify result
    //

    // <N>

    // flatMap()

    /**
     * Transform a value into a promise and get the resulting value.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise.flatMap(value -> Promise.get(() -> value + " world"));
     *     assertEquals("hello world", promise2.await());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public <N> Promise<N> flatMap(Function<R, Promise<N>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> handle
                .apply(from)
                .await()
        );
    }

    // <R>

    // flatPeek()

    /**
     * Start a new promise that uses result of this promise and passes result over.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.concurrent.atomic.AtomicReference;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var str = new AtomicReference<String>();
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise
     *         .flatPeek(value -> Promise.run(() -> str.set(value + " world")));
     *     assertEquals("hello", promise2.await());
     *     assertEquals("hello world", str.get());
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public Promise<R> flatPeek(Function<R, Promise<?>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> {
                handle
                    .apply(from)
                    .await(); // result ignored
                return from;
            }
        );
    }

    //
    // Flat discard result
    //

    // <R>

    // flatRun()

    /**
     * Start a new promise waiting for this promise and the supplied promise.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.concurrent.atomic.AtomicReference;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var str = new AtomicReference<String>();
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise
     *         .flatRun(() -> Promise.run(() -> str.set("world")));
     *     promise2.await();
     *     assertEquals("world", str.get());
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public Promise<R> flatRun(Supplier<Promise<?>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> {
                handle.get().await(); // result ignored
                return from;
            }
        );
    }

    // flatAfter()

    /**
     * Start a new promise waiting for this promise and the supplied stream of promises.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import net.thebugmc.async.barrier.Barrier;
     * import java.util.ArrayList;
     * import java.util.List;
     * import java.util.stream.Stream;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var list = new ArrayList<>();
     *     var barrier = new Barrier(2);
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise
     *         .flatAfter(() -> Stream.of(
     *             Promise.run(() -> {
     *                 list.add("hello");
     *                 barrier.await();
     *             }),
     *             Promise.run(() -> {
     *                 barrier.await();
     *                 list.add("world");
     *             })
     *         ));
     *     promise2.await();
     *     assertEquals(List.of("hello", "world"), list);
     * }
     *}
     *
     * @param handle Action to perform.
     */
    public Promise<R> flatAfter(Supplier<Stream<Promise<?>>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> {
                //noinspection ResultOfMethodCallIgnored
                handle.get()
                    .map(Promise::await) // ensure await() happens inside the asyncAction block
                    .toList();           // by calling toList and hence terminating the stream
                return from;
            }
        );
    }

    // <N>

    // flatGet()

    /**
     * Start a new promise supplying a new result after this promise and the supplied promise.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise
     *         .flatGet(() -> Promise.get(() -> "world"));
     *     assertEquals("world", promise2.await());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public <N> Promise<N> flatGet(Supplier<Promise<N>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> handle.get().await()
        );
    }

    // flatBatch()

    /**
     * Start a new promise waiting for the results from the supplied stream of promises after this
     * promise is {@link #settled() settled}.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.stream.Stream;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Promise.get(() -> "hello");
     *     var promise2 = promise
     *         .flatBatch(() -> Stream.of(
     *             Promise.get(() -> "hello"),
     *             Promise.get(() -> "world")
     *         ));
     *     assertEquals(List.of("hello", "world"), promise2.await().toList());
     * }
     *}
     *
     * @param handle Action to perform.
     * @param <N>    New result type.
     */
    public <N> Promise<Stream<N>> flatBatch(Supplier<Stream<Promise<N>>> handle) {
        return new Promise<>(
            VirtualThreadScheduler.TryParent.INSTANCE,
            Optional.of(this),
            from -> handle.get()
                .map(Promise::await) // ensure await() happens inside the asyncAction block
                .toList().stream()   // by calling toList and hence terminating the stream
        );
    }

    //
    // Stream collectors
    //

    /**
     * Collect all promises to await upon into a new promise.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.stream.Stream;
     * import java.util.concurrent.atomic.AtomicInteger;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var counter = new AtomicInteger();
     *     var promise = Stream
     *         .of(
     *             Promise.get(counter::incrementAndGet),
     *             Promise.get(counter::incrementAndGet)
     *         )
     *         .collect(Promise.toAfter());
     *     promise.await();
     *     assertEquals(2, counter.get());
     * }
     *}
     *
     * @return Collector to an awaiting promise.
     */
    public static Collector<Promise<?>, ArrayList<Promise<?>>, Promise<?>> toAfter() {
        return Collector.of(
            ArrayList::new,
            ArrayList::add,
            (p1, p2) -> {
                p1.addAll(p2);
                return p1;
            },
            Promise::after
        );
    }

    /**
     * Collect all results of the promise stream into a single result stream promise.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import net.thebugmc.async.Promise;
     * import net.thebugmc.async.schedule.VirtualThreadScheduler;
     * import java.util.stream.Stream;
     * import java.util.List;
     *
     * try (var scheduler = VirtualThreadScheduler.instance()) {
     *     var promise = Stream
     *         .of(
     *             Promise.get(() -> "hello"),
     *             Promise.get(() -> "world")
     *         )
     *         .collect(Promise.toBatch());
     *     assertEquals(List.of("hello", "world"), promise.await().toList());
     * }
     *}
     *
     * @param <N> Result type.
     * @return Collector to a result stream promise.
     */
    public static <N> Collector<Promise<N>, ArrayList<Promise<N>>, Promise<Stream<N>>> toBatch() {
        return Collector.of(
            ArrayList::new,
            ArrayList::add,
            (p1, p2) -> {
                p1.addAll(p2);
                return p1;
            },
            Promise::batch
        );
    }
}
