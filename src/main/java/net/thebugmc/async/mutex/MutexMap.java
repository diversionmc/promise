package net.thebugmc.async.mutex;

import net.thebugmc.async.Blocking;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * {@link MutexMap} is a {@link Mutex}-wrapper of a map and its values, which makes it
 * multithreading-safe while trying to perform the best.
 *
 * <p>
 * It is unable to match default {@link Map} interface, and does not support iteration. Use {@link
 * #access()} to get a {@link Mutex.Access} to the underlying {@link Map}.
 *
 * @param <K> Stored key type.
 * @param <V> Stored value type (in a {@link Mutex}).
 */
public class MutexMap<K, V> {
    private final Mutex<Map<K, Mutex<V>>> inner = Mutex.of(new HashMap<>());

    /**
     * Get a {@link Mutex.Access} to the underlying {@link Map}.
     *
     * @return {@link Mutex.Access}
     */
    @Blocking
    public Mutex<Map<K, Mutex<V>>>.Access access() {
        return inner.access();
    }

    /**
     * Put a value into the map.
     *
     * @param key   Key to store.
     * @param value Value to store.
     * @return The old value.
     */
    @Blocking
    public V put(K key, V value) {
        try (var access = access()) {
            var map = access.get();

            var put = map.put(key, Mutex.of(value));
            if (put == null)
                // noinspection ReturnOfNull
                return null; // default Java behavior I guess

            return put.take();
        }
    }

    /**
     * Put all key-value pairs into the map.
     *
     * @param pairs Key-value pairs to store. Each value will be wrapped in a {@link Mutex}.
     */
    @Blocking
    public void putAll(Map<K, V> pairs) {
        try (var access = access()) {
            var map = access.get();

            for (var pair : pairs.entrySet())
                map.put(pair.getKey(), Mutex.of(pair.getValue()));
        }
    }

    /**
     * Remove a value from the map.
     *
     * @param key Key of the entry.
     * @return The removed value.
     */
    @Blocking
    public V remove(K key) {
        try (var access = access()) {
            var map = access.get();

            var removed = map.remove(key);
            if (removed == null)
                // noinspection ReturnOfNull
                return null; // default Java behavior I guess

            return removed.take();
        }
    }

    /**
     * Get a value from the map.
     *
     * @param key Key of the entry.
     * @return The value or {@code null} if not found.
     */
    @Blocking
    public Mutex<V> get(K key) {
        try (var access = access()) {
            var map = access.get();
            return map.get(key); // mutex value is safe to escape mutex map
        }
    }

    /**
     * Get a value from the map or create a {@link Mutex} using the {@code defaultValue} if not
     * found.
     *
     * @param key Key of the entry.
     * @param defaultValue Function to create the value if not found.
     * @return The value or {@link Mutex#of()} if not found.
     */
    public Mutex<V> computeIfAbsent(K key, Function<K, V> defaultValue) {
        try (var access = access()) {
            var map = access.get();
            return map.computeIfAbsent(key, k -> Mutex.of(defaultValue.apply(k)));
        }
    }

    /**
     * Clear the map.
     */
    @Blocking
    public void clear() {
        try (var access = access()) {
            var map = access.get();
            map.clear();
        }
    }
}
