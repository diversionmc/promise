rootProject.name = "promise"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
    }
}